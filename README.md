# Colour Region Names

In 2010, Randall Munroe of xkcd created a Colour Survey. He posted [a
great analysis of the results][0]. He included a 'map' of the three
fully saturated faces of the RGB cube, which I found particularly
interesting, but was limited to only fully saturated colours. I wrote
some code to show the same for the full RGB space.

## How to Run

### Dependencies

- Python, pygame, tqdm, colour-science
- Rust, Cargo

### Procedure

1. Download the raw data file [here][1]. Extract it into the code
   directory.
2. Run `./sql_to_json.py` and `./luvs_in_srgb.py`.
3. Run `cargo run --release`.
4. Run `./picker.py`.

Execution time and resolution can be traded off by modifying `bits.txt`.

[0]: https://blog.xkcd.com/2010/05/03/color-survey-results/ "Colour Survey Results"
[1]: http://xkcd.com/color/colorsurvey.tar.gz "SQL Dump"
