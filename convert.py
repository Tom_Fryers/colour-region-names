from pathlib import Path

import colour
import numpy as np

colour.set_domain_range_scale("1")

max_l = 2 ** int(Path("bits.txt").read_text())
max_u = max_l
max_v = max_l


def luv_to_rgb(luv):
    return colour.XYZ_to_sRGB(
        np.nan_to_num(
            colour.Luv_to_XYZ(
                (luv + 0.5) * np.array((1 / max_l, 4 / max_u, 4 / max_v))
                - np.array((0, 2, 2))
            )
        )
    ).reshape(*luv.shape)
