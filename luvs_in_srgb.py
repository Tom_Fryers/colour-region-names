#!/usr/bin/env python3
import json
from pathlib import Path

import numpy as np

import convert

max_l = 2 ** int(Path("bits.txt").read_text())
max_u = max_l
max_v = max_l


def main():
    int_colours = np.indices((max_l, max_u, max_v)).T.reshape(-1, 3)

    rgb = convert.luv_to_rgb(int_colours)
    ok = int_colours[np.all((0 <= rgb) & (rgb <= 1), axis=1)]

    with Path("srgbs.json").open("w") as f:
        json.dump(ok.tolist(), f, separators=(",", ":"))


if __name__ == "__main__":
    main()
