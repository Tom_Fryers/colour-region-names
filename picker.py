#!/usr/bin/env python3
import argparse
import itertools
import json
from collections import Counter, defaultdict
from functools import reduce
from operator import add
from pathlib import Path

import colour
import numpy as np
import pygame
from tqdm import trange

import convert

max_l = 2 ** int(Path("bits.txt").read_text())
max_u = max_v = max_l

colour.set_domain_range_scale("1")


def add_tuples(tup1, tup2):
    return tuple(map(add, tup1, tup2))


def luv_to_rgb_256(c):
    return (np.clip(convert.luv_to_rgb(c), 0, 1) * 255).round().astype(np.int64)


def draw_level(l, scale):
    font = pygame.font.Font(None, round(max(max_u, max_v) * scale / 60))
    level_names_positions = defaultdict(list)
    valid_pairs = {(u, v): names[(l, u, v)] for u, v in grid if (l, u, v) in names}
    image = pygame.Surface((len(u_range) * scale, len(v_range) * scale))
    image.fill((119, 119, 119))
    if len(valid_pairs) < 2:
        return image
    rgbs = luv_to_rgb_256(np.array([(l, u, v) for u, v in valid_pairs]))
    for ((u, v), name), rgb in zip(valid_pairs.items(), rgbs):
        pygame.draw.rect(
            image,
            rgb,
            ((u - u_range.start) * scale, (v - v_range.start) * scale, scale, scale),
        )
        level_names_positions[name].append((u + 0.5, v + 0.5))

    line_colour = (255, 255, 255) if l <= max_l // 2 else (0, 0, 0)

    def draw_border(*points):
        pygame.draw.line(
            image,
            line_colour,
            *(
                [round(scale * c) for c in (p[0] - u_range.start, p[1] - v_range.start)]
                for p in points
            ),
            max(round(0.25 * scale), 1),
        )

    for pair in {(p[0] + 1, p[1] + 1) for p in valid_pairs}.union(valid_pairs):
        adjacent = {
            (-1, -1): valid_pairs.get((pair[0] - 1, pair[1] - 1)),
            (-1, 1): valid_pairs.get((pair[0] - 1, pair[1])),
            (1, -1): valid_pairs.get((pair[0], pair[1] - 1)),
            (1, 1): valid_pairs.get((pair[0], pair[1])),
        }
        if len(set(adjacent.values())) == 1:
            continue
        orthogonals = {(0, -1), (0, 1), (-1, 0), (1, 0)}
        for (x, y), name in adjacent.items():
            if (
                name != adjacent[(-x, y)] == adjacent[(x, -y)] is not None
                and name != adjacent[(-x, -y)]
                and name is not None
            ):
                points = ((0, y), (x, 0))
                draw_border(
                    *([pair[j] + points[i][j] / 2 for j in range(2)] for i in range(2)),
                )
                orthogonals.difference_update(points)

        for orthogonal in orthogonals:
            zero = orthogonal.index(0)
            adjacents = {
                adjacent[
                    tuple(orthogonal[j] + (i if j == zero else 0) for j in range(2))
                ]
                for i in (-1, 1)
            }
            if None not in adjacents and len(adjacents) == 2:
                draw_border(pair, [p + o / 2 for p, o in zip(pair, orthogonal)])

    for name, poses in level_names_positions.items():
        text = font.render(name, True, line_colour)
        position = add_tuples(
            (
                (x / len(poses) - o) * scale
                for x, o in zip(
                    reduce(add_tuples, poses), (u_range.start, v_range.start)
                )
            ),
            (-x / 2 for x in text.get_size()),
        )
        image.blit(text, position)
    return image


with Path("names.json").open() as f:
    names = json.load(f)
names = {tuple(c): n for c, n in names}
u_range = range(min(u for _, u, _ in names), max(u for _, u, _ in names) + 1)
v_range = range(min(v for _, _, v in names), max(v for _, _, v in names) + 1)

grid = tuple(itertools.product(u_range, v_range))


def picker(images, scale, size):
    def s(*values):
        return tuple(round(size * x) for x in values)

    S = pygame.display.set_mode(s(1.15, 1))
    pygame.display.set_icon(images[max_l // 2])

    showing = 0
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    showing += 1
                elif event.key == pygame.K_DOWN:
                    showing -= 1
        keys = pygame.key.get_pressed()
        showing += keys[pygame.K_PAGEUP] - keys[pygame.K_PAGEDOWN]
        showing %= max_l
        mp = pygame.mouse.get_pos()
        colour = (
            showing,
            mp[0] // scale + u_range.start,
            mp[1] // scale + v_range.start,
        )
        S.fill((119, 119, 119))
        S.blit(images[showing], (0, 0))
        pygame.draw.rect(S, (0, 0, 0), s(1, 0.1, 0.1, 0.05))
        pygame.draw.rect(S, (255, 255, 255), s(1, 0.15, 0.1, 0.05))
        if 0 <= colour[1] < max_u and 0 <= colour[2] < max_v:
            rgb = luv_to_rgb_256(np.array(colour))
            pygame.display.set_caption(
                f"{tuple(round(x) for x in rgb)}: {names.get(colour, 'N/A')}"
            )
            pygame.draw.rect(S, rgb, s(1.025, 0.125, 0.05, 0.05))

        pygame.display.flip()
        clock.tick(60)


def list_most_common():
    for n, c in Counter(names.values()).most_common():
        print(f"{100 * c / len(names):7.4f} {n}")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--most-common",
        action="store_true",
        help="output a list of the most common colours",
    )
    parser.add_argument(
        "-i",
        "--images",
        type=Path,
        help="save the generated images instead of showing the picker",
    )
    parser.add_argument("-s", "--size", type=int, default=900, help="window height")
    args = parser.parse_args()
    if args.most_common:
        list_most_common()
    scale = args.size // (max(len(u_range), len(v_range)))

    pygame.init()

    images = (draw_level(l, scale) for l in trange(max_l))
    if args.images:
        for i, image in enumerate(images):
            pygame.image.save(image, args.images / f"{i:03}.png")
        return
    images = tuple(images)

    picker(images, scale, args.size)


if __name__ == "__main__":
    main()
