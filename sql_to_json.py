#!/usr/bin/env python3
import json
import re
from pathlib import Path

import colour
import numpy as np

REGEX = r"VALUES\([0-9]*,[0-9]*,[0-9]*\.[0-9]*,([0-9]*),([0-9]*),([0-9]*),'([a-z0-9\- /]*)'\);"

colour.set_domain_range_scale("1")


def rgb_to_luv(c):
    return colour.XYZ_to_Luv(colour.sRGB_to_XYZ(c))


def main():
    data = Path("colorsurvey/mainsurvey_sqldump.txt").read_text()
    matches = re.finditer(REGEX, data)
    colours = []
    names = []
    for m in matches:
        colours.append((int(m.group(1)), int(m.group(2)), int(m.group(3))))
        names.append(m.group(4))
    luvs = rgb_to_luv(np.array(colours) / 255).tolist()

    with Path("luvnames.json").open("w") as f:
        json.dump(list(zip(luvs, names)), f, separators=(",", ":"))


if __name__ == "__main__":
    main()
