use serde::Deserialize;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;

const RADIUS: f64 = 0.15;

#[derive(Deserialize, Debug)]
struct OutputLuvColour {
    l: u32,
    a: u32,
    b: u32,
}
impl OutputLuvColour {
    fn to_luv(&self, max_val: u32) -> LuvColour {
        LuvColour {
            l: (self.l as f64 + 0.5) / max_val as f64,
            a: 4.0 * (self.a as f64 + 0.5) / (max_val) as f64 - 2.0,
            b: 4.0 * (self.b as f64 + 0.5) / (max_val) as f64 - 2.0,
        }
    }
}
#[derive(Deserialize, Debug, Copy, Clone)]
struct LuvColour {
    l: f64,
    a: f64,
    b: f64,
}

#[derive(Deserialize, Debug, Clone)]
struct NamedLuvColour {
    col: LuvColour,
    name: String,
}

impl LuvColour {
    fn diff(&self, other: &LuvColour) -> f64 {
        ((self.l - other.l).powi(2) + (self.a - other.a).powi(2) + (self.b - other.b).powi(2))
            .sqrt()
    }
}

fn get_cols() -> Vec<OutputLuvColour> {
    let file = File::open("srgbs.json").unwrap();
    let reader = BufReader::new(file);
    serde_json::from_reader(reader).unwrap()
}

fn get_names() -> Vec<NamedLuvColour> {
    let file = File::open("luvnames.json").unwrap();
    let reader = BufReader::new(file);
    serde_json::from_reader(reader).unwrap()
}

fn out(data: Vec<((u32, u32, u32), &str)>) {
    let file = File::create("names.json").unwrap();
    let writer = BufWriter::new(file);
    serde_json::to_writer(writer, &data).unwrap();
}

fn get_max_val() -> u32 {
    2_u32.pow(
        fs::read_to_string("bits.txt")
            .expect("missing bits.txt")
            .trim()
            .parse::<u32>()
            .expect("invalid bits.txt"),
    )
}
fn filter_rare_cols(data: Vec<NamedLuvColour>) -> Vec<NamedLuvColour> {
    let mut bad = HashMap::new();
    let mut good = HashSet::new();
    for value in &data {
        if good.contains(&value.name) {
            continue;
        }
        let c = bad.entry(value.name.clone()).or_insert(0);
        *c += 1;
        if *c == 2 {
            bad.remove_entry(&value.name).unwrap();
            good.insert(value.name.clone());
        }
    }
    data.into_iter()
        .filter(|x| !bad.contains_key(&x.name))
        .collect()
}
const L_BINS: usize = (1.0 / RADIUS) as usize + 1;
const U_BINS: usize = (4.0 / RADIUS) as usize + 1;
const V_BINS: usize = (4.0 / RADIUS) as usize + 1;
const TOTAL_BINS: usize = L_BINS * U_BINS * V_BINS;
fn l_bin(u: f64) -> usize {
    (u / RADIUS) as usize
}
fn u_bin(u: f64) -> usize {
    ((2.0 + u) / RADIUS) as usize
}
fn v_bin(u: f64) -> usize {
    ((2.0 + u) / RADIUS) as usize
}
fn colour_index(l_bin: usize, u_bin: usize, v_bin: usize) -> usize {
    l_bin * L_BINS * U_BINS + u_bin * U_BINS + v_bin
}
const PROGRESS_BAR_LENGTH: usize = 100;
fn main() {
    let max_val = get_max_val();
    let names = filter_rare_cols(get_names());
    let mut binned_names: [Vec<NamedLuvColour>; TOTAL_BINS] = [(); TOTAL_BINS].map(|_| Vec::new());
    for name in names {
        binned_names[colour_index(l_bin(name.col.l), u_bin(name.col.a), v_bin(name.col.b))]
            .push(name);
    }

    let cols = get_cols();
    let mut out_names = Vec::with_capacity(cols.len());
    let targets: HashSet<_> = (0..=PROGRESS_BAR_LENGTH)
        .map(|x| (x * cols.len() / PROGRESS_BAR_LENGTH))
        .collect();
    eprintln!("|{}|", "-".repeat(PROGRESS_BAR_LENGTH));
    eprint!("|");
    for (i, col) in cols.iter().enumerate() {
        let f_col = col.to_luv(max_val);
        if targets.contains(&i) {
            eprint!("#")
        }
        let mut name_weights = HashMap::new();
        let l_bin = l_bin(f_col.l);
        let u_bin = u_bin(f_col.a);
        let v_bin = v_bin(f_col.b);
        for new_l in l_bin.saturating_sub(1)..=(l_bin + 1).min(L_BINS - 1) {
            for new_a in u_bin.saturating_sub(1)..=(u_bin + 1).min(U_BINS - 1) {
                for new_b in v_bin.saturating_sub(1)..=(v_bin + 1).min(V_BINS - 1) {
                    for name in &binned_names[colour_index(new_l, new_a, new_b)] {
                        let weight = RADIUS - name.col.diff(&f_col);
                        if weight > 0.0 {
                            let w = name_weights.entry(&name.name).or_insert(0.0);
                            *w += weight;
                        }
                    }
                }
            }
        }
        if name_weights.is_empty() {
            continue;
        }
        out_names.push((
            (col.l, col.a, col.b),
            name_weights
                .iter()
                .max_by(|&(_, item1), &(_, item2)| item1.partial_cmp(item2).unwrap())
                .unwrap()
                .0
                .as_str(),
        ));
    }
    eprintln!("|");
    out(out_names);
}
